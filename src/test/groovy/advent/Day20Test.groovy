package advent

import spock.lang.Specification

/**
 * Created by joe on 20.12.17.
 */
class Day20Test extends Specification {

    def "test "() {
        given:
            def lines = new Dataset("Day20example.txt").getLines()
        when:
            def result = Day20.getClosestLongTermParticle(lines)
        then:
            result == 0
    }
}
