package advent

import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by joe on 03.12.17.
 */
class Day3Test extends Specification {

    @Unroll
    def "test guess #input"() {

        when:
            int result = Day3.guess(input);
        then:
            result == expectedResult

        where:
            input | expectedResult
            1024  | 31
            1     | 0
            12    | 3
            23    | 2
    }
}
