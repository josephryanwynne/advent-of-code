package advent

import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by joe on 11.12.17.
 */
class Day11Test extends Specification {

    @Unroll
    def "test get steps #input"() {
        when:
            def result = Day11.getSteps(input)
        then:
            result == expectedSteps
        where:
            input            | expectedSteps
            "ne,ne,ne"       | 3
            "ne,ne,sw,sw"    | 0
            "ne,ne,s,s"      | 2
            "se,sw,se,sw,sw" | 3

    }
}
