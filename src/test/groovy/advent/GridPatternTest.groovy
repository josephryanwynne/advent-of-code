package advent

import spock.lang.Specification

/**
 * Created by joe on 21.12.17.
 */
class GridPatternTest extends Specification {

    def "test gridpattern #pattern match"() {

        when:
            def pattern2 = new Day21.GridPattern2(pattern, "")

            int[] integers = comparison
        then:
            pattern2.matches(integers) == matches
        where:
            pattern | comparison   | matches
            "../.." | [0, 0, 0, 0] | true
            "##/##" | [1, 1, 1, 1] | true
            "##/.." | [0, 0, 1, 1] | true
            ".#/#." | [1, 0, 1, 0] | false
    }


    def "test gridpattern3 #pattern match"() {

        when:
            def pattern2 = new Day21.GridPattern2(pattern, "")

            int[] integers = comparison
        then:
            pattern2.matches(integers) == matches
        where:
            pattern | comparison   | matches
            "../.." | [0, 0, 0, 0] | true
            "##/##" | [1, 1, 1, 1] | true
            "##/.." | [0, 0, 1, 1] | true
            ".#/#." | [1, 0, 1, 0] | false
    }
}
