package advent

import spock.lang.Specification

import static advent.Day2.doOtherStuff

/**
 * Created by joe on 02.12.17.
 */
class Day2Test extends Specification {

//    def "test "() {
//        given:
//            int [][] input = new int [][]{{5 ,1, 9, 5},{7, 5 ,3},{2, 4 ,6 ,8}};
//
//        when:
//            def result = Day2.doStuff(input)
//        then:
//            result == 18
//    }

    def "test "() {
        given:
           int [][] input =  [[5 ,9, 2, 8],
                             [9 ,4, 7, 3],
                             [3 ,8, 6, 5]]
        when:
            def stuff = doOtherStuff(input)
        then:
            stuff == 9
    }
}
