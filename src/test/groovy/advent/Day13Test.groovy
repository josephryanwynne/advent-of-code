package advent

import spock.lang.Specification

/**
 * Created by joe on 13.12.17.
 */
class Day13Test extends Specification {


    def "test day13"() {
        given:
            List<String> input = Arrays.asList(("0: 3\n" +
                    "1: 2\n" +
                    "4: 4\n" +
                    "6: 4").split("\\n"))
            println input
        when:
            def severity = Day13.getTotalSeverity(input)
        then:
            severity == 24

    }
}
