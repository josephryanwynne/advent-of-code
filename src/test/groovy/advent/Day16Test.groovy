package advent

import spock.lang.Specification

/**
 * Created by joe on 16.12.17.
 */
class Day16Test extends Specification {

    def "test "() {
        given:
            char [] chars = ['a', 'b' , 'c', 'd', 'e'];
            String moves = "s1,x3/4,pe/b"
        when:
            char[] result = Day16.run(chars, moves)
        then:
            result == ['b','a','e','d','c']
    }
}
