package advent

import spock.lang.Specification

/**
 * Created by joe on 14.12.17.
 */
class Day14Test extends Specification {

    def "test Day14"() {
        given:
            String input = "flqrgnkx"
        when:
            def part1 = Day14.part1(Day14.getGrid(input))
        then:
            part1 == 8108
    }

    def "test part2"() {
        given:
            String input = "flqrgnkx"
        when:
            def result = Day14.part2(Day14.getGrid(input))
        then:
            result == 1242
    }
}
