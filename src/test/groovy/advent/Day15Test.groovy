package advent

import spock.lang.Specification

/**
 * Created by joe on 15.12.17.
 */
class Day15Test extends Specification {
    def "test part1"() {
        given:
            int startA = 65;
            int factorA = 16807;
            int startB = 8921;
            int factorB = 48271;
        when:
            def result = Day15.run(startA, factorA, startB, factorB)
        then:
            result == 588
    }

    def "test generate #a #b"() {
//        given:
//
//        when:
//
//        then:
//
//            where:
//                currentA|currentB
//                1092455    |430625591
//                1181022009 | 1233683848
//                245556042  |1431495498
//                1744312007 |  137874439
//                1352636452 |  285222916
    }

    def "test generateFirst"() {
        when:
            def generate = Day15.generate(65, 16807)
        then:
            generate == 1092455
    }

}
