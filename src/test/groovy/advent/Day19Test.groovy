package advent

import spock.lang.Specification


class Day19Test extends Specification {

    def "test"() {

        given:
            def lines = new Dataset("Day19example.txt").getLines();
        when:
            def result = Day19.run(lines)
        then:
            result == "ABCDEF"
    }
}
