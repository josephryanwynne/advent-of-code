package advent

import spock.lang.Unroll

/**
 * Created by joe on 09.12.17.
 */
class Day9Test extends spock.lang.Specification {

    @Unroll
    def "test score is #expectedScore for #input"() {
        when:
            int result = Day9.getScore(input)
        then:
            result == expectedScore
        where:
            input                           | expectedScore
            "{}"                            | 1
            "{{{}}}"                        | 6
            "{{},{}}"                       | 5
            "{{{},{},{{}}}}"                | 16
            "{<a>,<a>,<a>,<a>}"             | 1
            "{{<ab>},{<ab>},{<ab>},{<ab>}}" | 9
            "{{<!!>},{<!!>},{<!!>},{<!!>}}" | 9
            "{{<a!>},{<a!>},{<a!>},{<ab>}}" | 3
    }

    @Unroll
    def "part2 score is #expectedScore for #input"() {
        when:
            int result = Day9.getScore(input)
        then:
            result == expectedScore
        where:
            input                 | expectedScore
            "<>"                  | 0
            "<random characters>" | 17
            "<<<<>"               | 3
            "<{!>}>"              | 2
            "<!!>"                | 0
            "<!!!>>"              | 0
            "<{o\"i!a,<{i<a>"     | 10
    }
}
