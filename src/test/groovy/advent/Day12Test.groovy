package advent

import spock.lang.Specification

/**
 * Created by joe on 11.12.17.
 */
class Day12Test extends Specification {


    String testInput = "0 <-> 2\n" +
    "1 <-> 1\n" +
    "2 <-> 0, 3, 4\n" +
    "3 <-> 2, 4\n" +
    "4 <-> 2, 3, 6\n" +
    "5 <-> 6\n" +
    "6 <-> 4, 5";

    def "test #input"() {
        given:
            def strings = testInput.split("\n") as List
        when:
            def result = Day12.run(strings)
        then:
            result == 6
        where:
            input | expectation
            ""    | ""
    }
}
