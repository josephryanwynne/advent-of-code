package advent

import spock.lang.Specification

/**
 * Created by joe on 02.12.17.
 */
class Day1Test extends Specification {

    def "test "() {

        when:
            def sum = Day1.getSumOfConsecutiveMatches("1111", 1)
        then:
            sum == 4
    }

    def "test 2"() {

        when:
            def sum = Day1.getSumOfConsecutiveMatches(input, input.length() / 2 as int)
        then:
            sum == expectedSum
        where:
            input    | expectedSum
            "1212" | 6
            "1221" | 0
            "123425" | 4
            "123123" | 12
            "12131415" | 4
    }
}
