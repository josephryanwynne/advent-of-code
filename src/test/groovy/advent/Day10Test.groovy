package advent

import spock.lang.Specification
import spock.lang.Unroll

import java.util.stream.IntStream

import static advent.Day10.getSparseHash

/**
 * Created by joe on 09.12.17.
 */
class Day10Test extends Specification {

    @Unroll
    def "test #input"() {

        when:
            def hash = Day10.generateDay10Hash(input)
        then:
            hash == expectedHash
        where:
            input      | expectedHash
            ""         | "a2582a3a0e66e6e86e3812dcb672a272"
            "AoC 2017" | "33efeb34ea91902bb2f59c9920caa6cd"
            "1,2,3"    | "3efbe78a8d82f29979031a4aa0b16a9d"
            "1,2,4"    | "63960835bcdc130f0b66d7ff4f6a5a8e"

    }

    def "test getSparseHash contains all the right numbers"() {
        given:

            def array = IntStream.rangeClosed(0, 255).toArray();
            byte [] var = [ 1, 2, 3, 4, 5 ];
        when:
            int [] sparseHash = getSparseHash(IntStream.rangeClosed(0, 255).toArray(), var)
            Arrays.sort(sparseHash);
        then:
            for (int i = 0; i < array.length; i++) {
                array[i] == sparseHash[i]
            }

    }

    def "test xorSubArray"() {
        given:
            int [] array = [65 , 27 , 9 , 1 , 4 , 3 , 40 , 50 , 91 , 7 , 6 , 0 , 2 , 5 , 68 , 22]
        when:
            def xorResult = ArrayUtils.xorSubArray(array, 0, array.length)
        then:
            xorResult == 64
    }

    def "test getBytes"() {

        when:
            byte [] result = Day10.getBytes("1,2,3")
        then:
            result == [49,44,50,44,51,17, 31, 73, 47, 23] as byte[]
    }
}
