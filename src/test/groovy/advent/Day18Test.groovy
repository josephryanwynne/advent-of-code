package advent

import spock.lang.Specification

/**
 * Created by joe on 18.12.17.
 */
class Day18Test extends Specification {


    def "test Day18"() {
        given:
            def lines = new Dataset("Day18example.txt").getLines()
        when:
            def result = Day18.process(lines)
        then:
            result == 4
    }
}
