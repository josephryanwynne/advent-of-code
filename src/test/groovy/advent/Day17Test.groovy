package advent

import spock.lang.Specification

/**
 * Created by joe on 17.12.17.
 */
class Day17Test extends Specification {

    def "test Day17"() {

        when:
            def run = Day17.run(3)
        then:
            run == 638
    }
}
