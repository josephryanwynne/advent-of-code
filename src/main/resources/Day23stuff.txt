set b 99
set c b
jnz a 2
jnz 1 5
mul b 100
sub b -100000
set c b
sub c -17000
set f 1
set d 2
set e 2

set g d //
mul g e // d * e = b
sub g b // g - b needs to be zero
jnz g 2 // g needs to be zero here
set f 0 // f is set to zero
sub e -1
set g e
sub g b   // g = e - b
jnz g -8  // g needs to be zero



d doesn't change in the loop. Always 2
e is incremented every execution


loopDVal = get value of D


b = 99;
c = b;
if(a != 0){
    b *= 100
    b += 100_000
    c = b
    c += 17000
}


do {
    f = 1
    d = 2

    do {
        e = 2

        do {

           if((e * d) == b){
                f = 0
           }

           e = e + 1
           g = e - b

        } while( g != 0 );

        d += 1

        g = d - b

    } while( g != 0);

    if(f == 0){
        h += 1
    }

    g = b - c
    b += 17
} while(g != 0);


jnz g 2
jnz 1 3
sub b -17
jnz 1 -23