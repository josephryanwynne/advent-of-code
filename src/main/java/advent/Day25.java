package advent;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

/**
 * Created by joe on 02.12.17.
 */
public class Day25 {


    private static final int STEPS = 12586542;
    private static int currentPosition = 0;

    public static void main(String[] args) {

        Set<Integer> setPositions = new HashSet<>();
        State currentState = State.A;
        for (int i = 0; i < STEPS; i++) {
            currentState = currentState.function.apply(setPositions);
        }
        System.out.println("Number of set things: " + setPositions.size());
    }

    public enum State {

        A(Day25::doA),
        B(Day25::doB),
        C(Day25::doC),
        D(Day25::doD),
        E(Day25::doE),
        F(Day25::doF);

        Function<Set<Integer>, State> function;

        State(Function<Set<Integer>, State> function) {
            this.function = function;
        }
    }

    public static State doA(Set<Integer> setPositions){
        //    In state A:
        //    If the current value is 0:
        //            - Write the value 1.
        //            - Move one slot to the right.
        //            - Continue with state B.
        //    If the current value is 1:
        //            - Write the value 0.
        //            - Move one slot to the left.
        //            - Continue with state B.

        if(setPositions.contains(currentPosition)){
            setPositions.remove(currentPosition);
            currentPosition--;
            return State.B;
        } else {
            setPositions.add(currentPosition);
            currentPosition++;
            return State.B;
        }

    }


    public static State doB(Set<Integer> setPositions){
//    In state B:
//    If the current value is 0:
//            - Write the value 0.
//            - Move one slot to the right.
//            - Continue with state C.
//    If the current value is 1:
//            - Write the value 1.
//            - Move one slot to the left.
//            - Continue with state B.

        if(setPositions.contains(currentPosition)){
            setPositions.add(currentPosition);
            currentPosition--;
            return State.B;
        } else {
            setPositions.remove(currentPosition);
            currentPosition++;
            return State.C;
        }

    }


    public static State doC(Set<Integer> setPositions){
//    In state C:
//    If the current value is 0:
//            - Write the value 1.
//            - Move one slot to the right.
//            - Continue with state D.
//    If the current value is 1:
//            - Write the value 0.
//            - Move one slot to the left.
//            - Continue with state A.

        if(setPositions.contains(currentPosition)){
            setPositions.remove(currentPosition);
            currentPosition--;
            return State.A;
        } else {
            setPositions.add(currentPosition);
            currentPosition++;
            return State.D;
        }

    }
//


    public static State doD(Set<Integer> setPositions){
//    In state D:
//    If the current value is 0:
//            - Write the value 1.
//            - Move one slot to the left.
//            - Continue with state E.
//    If the current value is 1:
//            - Write the value 1.
//            - Move one slot to the left.
//            - Continue with state F.

        if(setPositions.contains(currentPosition)){
            setPositions.add(currentPosition);
            currentPosition--;
            return State.F;
        } else {
            setPositions.add(currentPosition);
            currentPosition--;
            return State.E;
        }

    }


    public static State doE(Set<Integer> setPositions){
//    In state E:
//    If the current value is 0:
//            - Write the value 1.
//            - Move one slot to the left.
//            - Continue with state A.
//    If the current value is 1:
//            - Write the value 0.
//            - Move one slot to the left.
//            - Continue with state D.

        if(setPositions.contains(currentPosition)){
            setPositions.remove(currentPosition);
            currentPosition--;
            return State.D;
        } else {
            setPositions.add(currentPosition);
            currentPosition--;
            return State.A;
        }

    }


    public static State doF(Set<Integer> setPositions){
//    In state F:
//    If the current value is 0:
//            - Write the value 1.
//            - Move one slot to the right.
//            - Continue with state A.
//    If the current value is 1:
//            - Write the value 1.
//            - Move one slot to the left.
//            - Continue with state E.

        if(setPositions.contains(currentPosition)){
            setPositions.add(currentPosition);
            currentPosition--;
            return State.E;
        } else {
            setPositions.add(currentPosition);
            currentPosition++;
            return State.A;
        }

    }

//

}
