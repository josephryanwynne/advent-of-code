package advent;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by joe on 02.12.17.
 */
public class Day6 {


    public static void main(String[] args) {
        String [] memoryBanks = new Dataset("Day6.txt").asSingleLineString().split("\\t");


        int [] banks = new int [memoryBanks.length];
//        int [] banks = new int []{0,2,7,0};
        for(int i = 0; i < memoryBanks.length;i++){
            banks[i] = Integer.parseInt(memoryBanks[i]);
        }

        int iterations = 0;
        Set<String> confs = new HashSet<>();
        String currentConf = Arrays.toString(banks);



        while(!confs.contains(currentConf)) {
            confs.add(currentConf);
            int maxIndex = findMaxIndex(banks);

            int valueAtMaxIndex = banks[maxIndex];
            banks[maxIndex] = 0;

            System.out.println("MaxIndex " + maxIndex);
            System.out.println("MaxVal " + valueAtMaxIndex);

            for (int i = 1; i <= valueAtMaxIndex; i++){
                banks[(maxIndex+i) % banks.length] ++;
            }
            currentConf = Arrays.toString(banks);
            iterations++;
            System.out.println(Arrays.toString(banks));
        }

        iterations = 0;
        confs = new HashSet<>();
        confs.add(currentConf);
        currentConf = "BLAG";
        while(!confs.contains(currentConf)) {
            confs.add(currentConf);
            int maxIndex = findMaxIndex(banks);

            int valueAtMaxIndex = banks[maxIndex];
            banks[maxIndex] = 0;

            System.out.println("MaxIndex " + maxIndex);
            System.out.println("MaxVal " + valueAtMaxIndex);

            for (int i = 1; i <= valueAtMaxIndex; i++){
                banks[(maxIndex+i) % banks.length] ++;
            }
            currentConf = Arrays.toString(banks);
            iterations++;
            System.out.println(Arrays.toString(banks));
        }

        System.out.println(iterations);
    }

    private static int findMaxIndex(int[] banks) {

        int maxIndex = Integer.MIN_VALUE, maxVal = Integer.MIN_VALUE;

        for(int i = 0; i < banks.length; i++){

            if(banks[i] > maxVal){
                maxIndex = i;
                maxVal = banks[i];
                System.out.println(banks[i] + " was bigger");
            }
        }

        return maxIndex;
    }
}
