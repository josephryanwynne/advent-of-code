package advent;

/**
 * Created by joe on 23.12.17.
 */
public class Day23Java {


    public static void main(String[] args) {

        long a = 1, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0, h = 0;
        a = 1;
        b = 99;
        c = b;
        if(a != 0){
            b *= 100;
            b += 100_000;
            c = b;
            c += 17000;
        }


        do {
            f = 1;
            d = 2;

            do {
                e = 2;

                do {

                    if(e > b){
                        throw new RuntimeException("Didn't expect that!");
                    }

                    if((b % d) == 0){
                        if(b / e != 0){
                            f = 0;
                        }
                    }

                    e = b;
                    g = e - b;

                } while( g != 0 );

                d += 1;

                g = d - b;

            } while( g != 0);

            if(f == 0){

                h += 1;
                System.out.println("h = " + h);
            }

            g = b - c;
            b += 17;
        } while(g != 0);


    }
}
