package advent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by joe on 02.12.17.
 */
public class Day17 {


    public static void main(String[] args) throws IOException {

        //System.in.read();

        long time = System.currentTimeMillis();
        int nextValue = run(380);
        System.out.println(nextValue);
        System.out.println("Duration : " + (System.currentTimeMillis() - time));
    }

    public static int run(int stepSize) {

        ArrayList<Integer> buffer = new ArrayList<>();
        buffer.add(0);
        int numSteps = 50_000_000;

        int currentPos = 0;
        int currentZeroPos = 0;
        int theoreticalBufferSize = 1;
        int thingAfter0 = -1;
        for (int i = 1; i <= numSteps; i++) {


           // buffer.iterator().remove();
//            System.out.println("CurrentPos : " + currentPos);
            int nextPos;
            if(i == 0) {
                nextPos = 1;
//                System.out.println("This is zero so it's special.");
            } else if (buffer.size() < stepSize) {
                int remainder = stepSize % theoreticalBufferSize;
                nextPos = (currentPos + remainder) % theoreticalBufferSize + 1;
//                System.out.println(String.format("Looped Mod : Divisor[%s] Remainder[%s] NextPos[%s]", divisor, remainder, nextPos));

            } else {
                nextPos = ((currentPos + stepSize) % theoreticalBufferSize) + 1;
//                System.out.println(String.format("Simple mod + 1 : NextPos[%s]", nextPos));
            }


//            System.out.println(String.format("Number[%s] Pos[%s] NextPos[%s]", i, currentPos, nextPos));
            if(nextPos < currentZeroPos) {
                currentZeroPos ++;
            } else if(nextPos == (currentZeroPos + 1)) {

                thingAfter0 = i;
            }
            currentPos = nextPos;

            theoreticalBufferSize ++;
//            buffer.set()
//            System.out.println(buffer);
//            System.out.println();
//            if(i > 100) break;

        }

//        int nextIndex = buffer.indexOf(0) + 1;
//        Integer integer = buffer.get(nextIndex);

        return thingAfter0;
    }
}
