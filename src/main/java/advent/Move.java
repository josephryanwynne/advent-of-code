package advent;

/**
 * Created by joe on 16.12.17.
 */
public abstract class Move {

    public abstract char[] apply(char [] input);

    public static class PositionSwap extends Move {

        private final int posA, posB;

        public PositionSwap(int posA, int posB) {
            this.posA = posA;
            this.posB = posB;
        }

        @Override
        public char[] apply(char[] input) {
            char [] newArray = new char[input.length];
            System.arraycopy(input, 0, newArray, 0, input.length);

            char tmp = newArray[posA];
            newArray[posA] = newArray[posB];
            newArray[posB] = tmp;
            return newArray;
        }

        @Override
        public String toString() {
            return "PositionSwap{" +
                    "posA=" + posA +
                    ", posB=" + posB +
                    '}';
        }
    }

    public static class ValueSwap extends Move {
        private final char charA, charB;

        public ValueSwap(char charA, char charB) {
            this.charA = charA;
            this.charB = charB;
        }

        @Override
        public char[] apply(char[] input) {
            char [] newArray = new char[input.length];
            System.arraycopy(input, 0, newArray, 0, input.length);
            int posA = -1, posB = -1;
            for (int i = 0; i < input.length; i++) {
                if(input[i] == charA){
                    posA = i;
                } else if(input[i] == charB){
                    posB = i;
                }
                if(posA != -1 && posB != -1) break;
            }

            char tmp = newArray[posA];
            newArray[posA] = newArray[posB];
            newArray[posB] = tmp;
            return newArray;
        }

        @Override
        public String toString() {
            return "ValueSwap{" +
                    "charA=" + charA +
                    ", charB=" + charB +
                    '}';
        }
    }

    public static class Spin extends Move {

        private final int elementsToMove;

        public Spin(int numElementsToSpin) {
            this.elementsToMove = numElementsToSpin;
        }

        @Override
        public char[] apply(char[] input) {
            char [] newArray = new char[input.length];
            System.arraycopy(input, input.length - elementsToMove, newArray, 0, elementsToMove);
            System.arraycopy(input, 0, newArray, elementsToMove, input.length - elementsToMove);
            return newArray;
        }

        @Override
        public String toString() {
            return "Spin{" +
                    "elementsToMove=" + elementsToMove +
                    '}';
        }
    }

}
