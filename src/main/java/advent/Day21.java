package advent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by joe on 02.12.17.
 */
public class Day21 {

    // Higher than 2204342

    public static void main(String[] args) {

        List<String> lines = new Dataset("Day21.txt").getLines();


        List<GridPattern> gridPatterns = getGridRules(lines);
        System.out.println("Found " + gridPatterns.size() + " rules");

        int[][] fractalGrid = applyRules(gridPatterns, 18);

        int total = 0;
        for (int i = 0; i < fractalGrid.length; i++) {
            int[] ints = fractalGrid[i];
            for (int j = 0; j < ints.length; j++) {
                total += ints[j];
            }
        }

        System.out.println("Total: " + total);
    }

    private static int[][] applyRules(List<GridPattern> gridPatterns, int iterations) {
//        .#.
//        ..#
//        ###
        int[][] grid = new int[3][];
        grid[0] = new int[]{0,1,0};
        grid[1] = new int[]{0,0,1};
        grid[2] = new int[]{1,1,1};

        int[][] currentGrid = grid;


        Set<String> seen = new HashSet<>();

        for(int i = 0; i < iterations; i ++){

            if(currentGrid.length % 2 == 0){

                // split by twos and process
                int newGridSize = currentGrid.length / 2 * 3;
                int[][] newGrid = new int [newGridSize][newGridSize];
                // split by threes and process
                int newGridX = 0;
                int newGridY = 0;

                for (int j = 0; j < currentGrid.length; j+=2) {

                    for (int k = 0; k < currentGrid.length; k+=2) {

                        int [] values = {
                                currentGrid[j]  [k],
                                currentGrid[j]  [k+1],
                                currentGrid[j+1][k+1],
                                currentGrid[j+1][k]
                        };
                        for (GridPattern pattern : gridPatterns) {

                            if(pattern.matches(values)){
                                seen.add(pattern.key);
                                int[][] replacement = pattern.getReplacement();

                                newGrid[newGridX][newGridY] = replacement[0][0];
                                newGrid[newGridX][newGridY+1] = replacement[0][1];
                                newGrid[newGridX][newGridY+2] = replacement[0][2];

                                newGrid[newGridX+1][newGridY] = replacement  [1][0];
                                newGrid[newGridX+1][newGridY+1] = replacement[1][1];
                                newGrid[newGridX+1][newGridY+2] = replacement[1][2];

                                newGrid[newGridX+2][newGridY] = replacement  [2][0];
                                newGrid[newGridX+2][newGridY+1] = replacement[2][1];
                                newGrid[newGridX+2][newGridY+2] = replacement[2][2];

                            }

                        }
                        newGridY += 3;

                    }

                    newGridX += 3;
                    newGridY = 0;
                }
                currentGrid = newGrid;


            } else {
                int newGridSize = currentGrid.length / 3 * 4;
                int[][] newGrid = new int [newGridSize][newGridSize];
                // split by threes and process
                int newGridX = 0;
                int newGridY = 0;


                for (int j = 0; j < currentGrid.length; j+=3) {

                    for (int k = 0; k < currentGrid.length; k+=3) {

                        int [] values = {
                                currentGrid[j]  [k],
                                currentGrid[j]  [k+1],
                                currentGrid[j]  [k+2],
                                currentGrid[j+1][k+2],
                                currentGrid[j+2][k+2],
                                currentGrid[j+2][k+1],
                                currentGrid[j+2][k],
                                currentGrid[j+1][k]
                        };

                        for (GridPattern pattern : gridPatterns) {
                            boolean grid3 = pattern instanceof GridPattern3;

                            if(!grid3) continue;

                            boolean middle = ((GridPattern3) pattern).middle == currentGrid[j + 1][k + 1];

                            if(middle && pattern.matches(values)){


                                System.out.println("------------");
                                for (int l = 0; l < pattern.keyParts.length; l++) {
                                    System.out.println(pattern.keyParts[l]);
                                }
                                System.out.println(String.format("%s%s%s",values[0], values[1], values[2] ));
                                System.out.println(String.format("%s%s%s",values[3], "?", values[4] ));
                                System.out.println(String.format("%s%s%s",values[5], values[6], values[7] ));
//                                sout
//                                for (int l = 0; l < pattern.keyParts.length; l++) {
//                                    System.out.println();
//
//                                }
//                                System.out.println(Arrays.asList(values));


                                seen.add(pattern.key);

                                int[][] replacement = pattern.getReplacement();

                                newGrid[newGridX][newGridY] = replacement[0][0];
                                newGrid[newGridX][newGridY+1] = replacement[0][1];
                                newGrid[newGridX][newGridY+2] = replacement[0][2];
                                newGrid[newGridX][newGridY+3] = replacement[0][3];

                                newGrid[newGridX+1][newGridY] = replacement  [1][0];
                                newGrid[newGridX+1][newGridY+1] = replacement[1][1];
                                newGrid[newGridX+1][newGridY+2] = replacement[1][2];
                                newGrid[newGridX+1][newGridY+3] = replacement[1][3];

                                newGrid[newGridX+2][newGridY] = replacement  [2][0];
                                newGrid[newGridX+2][newGridY+1] = replacement[2][1];
                                newGrid[newGridX+2][newGridY+2] = replacement[2][2];
                                newGrid[newGridX+2][newGridY+3] = replacement[2][3];

                                newGrid[newGridX+3][newGridY] = replacement  [3][0];
                                newGrid[newGridX+3][newGridY+1] = replacement[3][1];
                                newGrid[newGridX+3][newGridY+2] = replacement[3][2];
                                newGrid[newGridX+3][newGridY+3] = replacement[3][3];

                            }

                        }
                        newGridY += 4;

                    }

                    newGridX += 4;
                    newGridY = 0;
                }
                currentGrid = newGrid;
            }


//            for (int j = 0; j < currentGrid.length; j++) {
//                System.out.println(Arrays.toString(currentGrid[j]));
//            }
        }
        System.out.println(seen);

        return currentGrid;
    }

    public static List<GridPattern> getGridRules(List<String> lines){

        List<GridPattern> ruleset = new ArrayList<>();
        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            String[] split = line.split(" => ");
            String key = split[0];
            String value = split[1];

            String [] keyParts = key.split("/");
            if(keyParts.length == 2){
                ruleset.add(new GridPattern2(key, value));
            } else {
                ruleset.add(new GridPattern3(key, value));
            }

        }
       // System.out.println(ruleset);
        return ruleset;
    }

    private static abstract class GridPattern {
        public String key;
        public String[] keyParts;

        abstract int getSize();
        abstract boolean matches(int [] other);
        abstract int[][] getReplacement();
    }

    private static class GridPattern2 extends GridPattern {

        int [][] rotations = new int [4][];
        int [][] reversedRotations = new int [4][];

        String replacement;

        public GridPattern2(String key, String value) {
            this.keyParts = key.split("/");
            this.key = key;
            this.replacement = value;

            String keyWithoutSlashes = key.replaceAll("/", "");
            int [] keyAsArray = {
                    keyWithoutSlashes.charAt(0) == '#' ? 1:0,
                    keyWithoutSlashes.charAt(1) == '#' ? 1:0,
                    keyWithoutSlashes.charAt(3) == '#' ? 1:0,
                    keyWithoutSlashes.charAt(2) == '#' ? 1:0,

            };
            System.out.println(Arrays.toString(keyAsArray));
            for (int i = 0; i < keyAsArray.length; i++) {

                int[] rotation = {
                    keyAsArray[(0+i)%4],
                    keyAsArray[(1+i)%4],
                    keyAsArray[(2+i)%4],
                    keyAsArray[(3+i)%4]
                };

                rotations[i] = rotation;
            }

            int [] reversed = {
                    keyAsArray[1],
                    keyAsArray[0],
                    keyAsArray[3],
                    keyAsArray[2]
            };

            for (int i = 0; i < keyAsArray.length; i++) {

                int[] rotation = {
                    reversed[(0+i)%4],
                    reversed[(1+i)%4],
                    reversed[(2+i)%4],
                    reversed[(3+i)%4]
                };

                reversedRotations[i] = rotation;
            }

        }

        @Override
        int getSize() {
            return 2;
        }


        public boolean matches(int [] other) {

            for (int i = 0; i < rotations.length; i++) {
                if(Arrays.equals(rotations[i], other)){
                    System.out.println(String.format("ReversedRotation %s of %s matches %s", i, key, Arrays.toString(other)));
                    return true;
                }
            }

            for (int i = 0; i < reversedRotations.length; i++) {
                if(Arrays.equals(reversedRotations[i], other)){
                    System.out.println(String.format("ReversedRotation %s of %s matches %s", i, key, Arrays.toString(other)));
                    return true;
                }
            }

            return false;
        }

        @Override
        int[][] getReplacement() {
            String[] replacementParts = replacement.split("/");
            int [][] replacementGrid = new int[replacementParts.length][replacementParts.length];
            for (int i = 0; i < replacementParts.length; i++) {
                for (int j = 0; j < replacementParts[i].length(); j++) {
                    replacementGrid[i][j] = replacementParts[i].charAt(j) == '#' ? 1 : 0;
                }
            }
            return replacementGrid;
        }

        @Override
        public String toString() {
            return "GridPattern2{" +
                    "\n\trotations=" + Arrays.stream(rotations).map(Arrays::toString).collect(Collectors.joining(",")) +
                    "\n\treversedRotations=" + Arrays.stream(reversedRotations).map(Arrays::toString).collect(Collectors.joining(",")) +
                    "\n\treplacement='" + replacement + '\'' +
                    "}\n";
        }
    }

    public static class GridPattern3 extends GridPattern {

        private final int middle;
        int [][] rotations = new int [4][];
        int [][] reversedRotations = new int [4][];

        String replacement;

        public GridPattern3(String key, String value) {
            this.keyParts = key.split("/");
            this.key = key;
            this.replacement = value;
            String keyWithoutSlashes = key.replaceAll("/", "");

            this.middle = keyWithoutSlashes.charAt(4) == '#' ? 1:0;

            if(this.middle == 1){
                System.out.println("Middle: " + key);
            }
            int [] keyAsArray = {
                    keyWithoutSlashes.charAt(0) == '#' ? 1:0,
                    keyWithoutSlashes.charAt(1) == '#' ? 1:0,
                    keyWithoutSlashes.charAt(2) == '#' ? 1:0,
                    keyWithoutSlashes.charAt(5) == '#' ? 1:0,
                    keyWithoutSlashes.charAt(8) == '#' ? 1:0,
                    keyWithoutSlashes.charAt(7) == '#' ? 1:0,
                    keyWithoutSlashes.charAt(6) == '#' ? 1:0,
                    keyWithoutSlashes.charAt(3) == '#' ? 1:0

            };

            for (int i = 0; i < keyAsArray.length; i+=2) {

                int[] rotation = {
                        keyAsArray[(0+i)%8],
                        keyAsArray[(1+i)%8],
                        keyAsArray[(2+i)%8],
                        keyAsArray[(3+i)%8],
                        keyAsArray[(4+i)%8],
                        keyAsArray[(5+i)%8],
                        keyAsArray[(6+i)%8],
                        keyAsArray[(7+i)%8]
                };

                rotations[i/2] = rotation;
            }

            int [] reversed = {
                keyWithoutSlashes.charAt(2) == '#' ? 1:0,
                keyWithoutSlashes.charAt(1) == '#' ? 1:0,
                keyWithoutSlashes.charAt(0) == '#' ? 1:0,
                keyWithoutSlashes.charAt(3) == '#' ? 1:0,
                keyWithoutSlashes.charAt(6) == '#' ? 1:0,
                keyWithoutSlashes.charAt(7) == '#' ? 1:0,
                keyWithoutSlashes.charAt(8) == '#' ? 1:0,
                keyWithoutSlashes.charAt(5) == '#' ? 1:0
            };

            // This could also be completely wrong
            for (int i = 0; i < keyAsArray.length; i+=2) {

                int[] rotation = {
                        reversed[(0+i)%8],
                        reversed[(1+i)%8],
                        reversed[(2+i)%8],
                        reversed[(3+i)%8],
                        reversed[(4+i)%8],
                        reversed[(5+i)%8],
                        reversed[(6+i)%8],
                        reversed[(7+i)%8]
                };

                reversedRotations[i/2] = rotation;
            }

        }

        @Override
        int getSize() {
            return 3;
        }

        public boolean matches(int [] other) {

            for (int i = 0; i < rotations.length; i++) {
                //System.out.println(Arrays.toString(rotations[i]));
                if(Arrays.equals(rotations[i], other)){
                    System.out.println(String.format("Rotation %s %s of %s matches %s", i, Arrays.toString(rotations[i]), key, Arrays.toString(other)));
                    return true;
                }
            }

            for (int i = 0; i < reversedRotations.length; i++) {
                if(Arrays.equals(reversedRotations[i], other)){
                    System.out.println(String.format("ReversedRotation %s of %s matches %s", i, key, Arrays.toString(other)));
                    return true;
                }
            }

            return false;
        }

        @Override
        int[][] getReplacement() {
            String[] replacementParts = replacement.split("/");
            int [][] replacementGrid = new int[replacementParts.length][replacementParts.length];
            for (int i = 0; i < replacementParts.length; i++) {
                for (int j = 0; j < replacementParts[i].length(); j++) {
                    replacementGrid[i][j] = replacementParts[i].charAt(j) == '#' ? 1 : 0;
                }
            }
            return replacementGrid;
        }

        @Override
        public String toString() {
            return "GridPattern3{" +
                    "\n\trotations=" + Arrays.stream(rotations).map(Arrays::toString).collect(Collectors.joining(",")) +
                    "\n\treversedRotations=" + Arrays.stream(reversedRotations).map(Arrays::toString).collect(Collectors.joining(",")) +
                    "\n\treplacement='" + replacement + '\'' +
                    "}\n";
        }
    }
}
