package advent;

import java.util.Arrays;

/**
 * Created by joe on 10.12.17.
 */
public class ArrayUtils {

    public static void swap(int[] array, int swapPosA, int swapPosB) {
        int tmp = array[swapPosA];
        array[swapPosA] = (array[swapPosB]);
        array[swapPosB] = tmp;
    }


    public static int xorSubArray(int[] sparseHash, int startInclusive, int endExclusive) {
        return Arrays.stream(sparseHash, startInclusive, endExclusive).reduce((a, b) -> a ^ b).getAsInt();
    }
}
