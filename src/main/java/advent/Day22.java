package advent;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joe on 02.12.17.
 */
public class Day22 {

    //2488038 is too low

    public static void main(String[] args) {

        List<String> lines = new Dataset("Day22.txt").getLines();
        Cluster cluster = new Cluster(lines);
        cluster.printGrid();

        int numberOfMoves = 10_000_000;
        for (int i = 0; i < numberOfMoves; i++) {
            cluster.run();
            if(i < 100){
                cluster.printGrid();
            }
        }

        System.out.println(cluster);
    }

    private static class Cluster {

        char [][] grid;
        Move direction = Move.UP;
        int x, y;
        int newInfections = 0;
        int initialInfections = 0;

        Map<String, Node> nodeStore = new HashMap<>();

        public Cluster(List<String> lines) {

            grid = new char[lines.size()][lines.size()];

            for (int i = 0; i < lines.size(); i++) {
                for (int j = 0; j < lines.size(); j++) {
                    String s = lines.get(j);
                    char nodeStatus = s.charAt(i);
                    if(nodeStatus == '#'){
                        initialInfections ++;
                        nodeStore.put(getCoordinate(j, i), new Node(j, i, NodeState.INFECTED));
                    }
                    grid[j][i] = nodeStatus;
                }
            }

            x = lines.get(0).length() / 2;
            y = lines.size() / 2;

//            System.out.println("Starting at " + x + " " + y);
        }

        private String getCoordinate(int j, int i) {
            return String.format("%s,%s", j, i);
        }

        public void run (){
            String coordinate = getCoordinate(x, y);

            Node currentNode;
            if(nodeStore.containsKey(coordinate)){
                currentNode = nodeStore.get(coordinate);
                switch(currentNode.state){
                    case WEAKENED:
                        newInfections++;
                        //Do nothing
                        break;

                    case INFECTED:
                        direction = direction.getRightSpiralDirection();
                        break;

                    case FLAGGED:
                        direction = direction.getOppositeDirection();
                        nodeStore.remove(coordinate);
                        break;
                    default:
                        throw new RuntimeException("What are you doing here?! " + currentNode);
                }
                currentNode.state = currentNode.state.next();
            } else {
                // Node isn't there so must be clean, weaken it.
                currentNode = new Node(x, y, NodeState.WEAKENED);
                nodeStore.put(coordinate, currentNode);
                direction = direction.getLeftSpiralDirection();
            }

            if(grid[0].length > y && y >= 0 && grid.length > x && x >= 0){
                grid[x][y] = currentNode.state.character;
            }
            x += direction.xShift;
            y += direction.yShift;
        }

        public void printGrid(){

            for (char[] c : grid) {
                System.out.println(Arrays.toString(c));
            }
            System.out.println("--------------------");
        }

        @Override
        public String toString() {
            return "Cluster{" +
                    "grid=" + Arrays.toString(grid) +
                    "\n direction=" + direction +
                    "\n x=" + x +
                    "\n y=" + y +
                    "\n newInfections=" + newInfections +
                    "\n initialInfections=" + initialInfections +
                    '}';
        }
    }

    private static class Node {

        int x;
        int y;
        NodeState state;


        public Node(int x, int y, NodeState state) {
            this.x = x;
            this.y = y;
            this.state = state;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node that = (Node) o;

            if (x != that.x) return false;
            return y == that.y;

        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }

        @Override
        public String toString() {
            return "Coordinate{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

    public enum NodeState {

        CLEAN('.'),WEAKENED('W'),INFECTED('#'),FLAGGED('F');

        public char character;

        NodeState(char character) {
            this.character = character;
        }

        public NodeState next(){
            return NodeState.values()[(this.ordinal() + 1) % NodeState.values().length];
        }

    }

    private enum Move {

        RIGHT(0, 1, '-'), UP(-1, 0, '|'), LEFT(0, -1, '-'), DOWN(1, 0, '|');

        private final char representation;
        private int xShift;
        private int yShift;

        Move(int xShift, int yShift, char representation) {
            this.xShift = xShift;
            this.yShift = yShift;
            this.representation = representation;
        }

        List<Move> getSides(){
            return Arrays.asList(
                    values()[(this.ordinal()+values().length-1) % values().length],
                    values()[(this.ordinal()+1) % values().length]
            );
        }

        Move getLeftSpiralDirection(){
            return Move.values()[(this.ordinal() + 1) % Move.values().length];
        }
        Move getRightSpiralDirection(){
            return Move.values()[(this.ordinal() + Move.values().length - 1) % Move.values().length];
        }
        Move getOppositeDirection(){
            return Move.values()[(this.ordinal() + Move.values().length + 2) % Move.values().length];
        }
    }


}
