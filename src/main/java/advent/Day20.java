package advent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by joe on 02.12.17.
 */
public class Day20 {


    public static void main(String[] args) {

        List<String> lines = new Dataset("Day20.txt").getLines();

        long closest = getClosestLongTermParticle(lines);
        System.out.println(closest);
    }

    public static long getClosestLongTermParticle(List<String> lines) {

        List<Particle> particles = new ArrayList<>();
        Vector smallestAcceleration = new Vector(0, 0, 0);
        long smallestAccelerationScale = Long.MAX_VALUE;
        long smallestAccelerationIndex = -1;
        for (int i = 0; i < lines.size(); i++) {


            String line = lines.get(i);
            String[] parts = line.split("\\>, ");
            System.out.println(Arrays.toString(parts));
            Vector pos = getVector(parts[0]);
            Vector velocity = getVector(parts[1]);
            Vector acceleration = getVector(parts[2]);

            Particle newParticle = new Particle(pos, velocity, acceleration);
            particles.add(newParticle);

            long vectorScale = getVectorScale(newParticle.acceleration);
            //Probably wrong
//            if(vectorScale < smallestAccelerationScale ){
//
//                smallestAccelerationIndex = i;
//                smallestAccelerationScale = vectorScale;
//
//            } else if(vectorScale == smallestAccelerationScale){
//                throw new RuntimeException(String.format("%s is same as %s", i, smallestAccelerationIndex));
//            }

        }

        long finalClosestIndex = -1;
        //Not really sure about this
        while (true) {

            int closestIndex = -1;
            long closestManhattan = Long.MAX_VALUE;
            for (int i = 0; i < particles.size(); i++) {

                Particle particle = particles.get(i);
                particle.accelerate();
                particle.move();

                long manhattan = getVectorScale(particle.pos);

                if(manhattan < closestManhattan){
                    closestManhattan = manhattan;
                    closestIndex = i;
                    finalClosestIndex = closestIndex;
                }

            }

            Set<Particle> collidingParticles = new HashSet<>();

            for (int i = 0; i < particles.size() - 1; i++) {

                Particle particle = particles.get(i);

                for (int j = i+1; j < particles.size(); j++) {
                    Particle particle2 = particles.get(j);
                    if(particle.equals(particle2)){
                        collidingParticles.add(particle);
                        collidingParticles.add(particle2);
                    }

                }
            }
            particles.removeAll(collidingParticles);

            System.out.println("CurrentClosestParticle: " + finalClosestIndex);
            System.out.println("RemainingParticleCount: " + particles.size());
            if(false) break;// shut up compiler

        }


        System.out.println(particles);

        return smallestAccelerationIndex;
    }

    private static long getVectorScale(Vector acceleration) {
        return Math.abs(acceleration.x)
                + Math.abs(acceleration.y)
                + Math.abs(acceleration.z);
    }

    private static Vector getVector(String part) {
        String substring = part.substring(part.indexOf("<") + 1, part.length());
        String[] xyz = substring.split(",");
        //System.out.println(Arrays.toString(xyz));
        return new Vector(
                Long.parseLong(xyz[0].trim()),
                Long.parseLong(xyz[1].trim()),
                Long.parseLong(xyz[2].replaceAll("[v<>= ]", ""))
        );
    }

    private static class Particle {
        private Vector pos;
        private Vector velocity;
        private Vector acceleration;

        public Particle(Vector pos, Vector velocity, Vector acceleration) {
            this.pos = pos;
            this.velocity = velocity;
            this.acceleration = acceleration;
        }

        @Override
        public String toString() {
            return "Particle{" +
                    "pos=" + pos +
                    ", velocity=" + velocity +
                    ", acceleration=" + acceleration +
                    '}';
        }

        public void accelerate() {
            velocity.x += acceleration.x;
            velocity.y += acceleration.y;
            velocity.z += acceleration.z;
        }

        public void move() {
            pos.x += velocity.x;
            pos.y += velocity.y;
            pos.z += velocity.z;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Particle particle = (Particle) o;

            if (pos != null ? !pos.equals(particle.pos) : particle.pos != null) return false; else return true;

        }

        @Override
        public int hashCode() {
            int result = pos != null ? pos.hashCode() : 0;
            result = 31 * result + (velocity != null ? velocity.hashCode() : 0);
            result = 31 * result + (acceleration != null ? acceleration.hashCode() : 0);
            return result;
        }
    }

    public static class Vector {
        private long x;
        private long y;
        private long z;

        public Vector(long x, long y, long z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        @Override
        public String toString() {
            return "Vector{" +
                    "x=" + x +
                    ", y=" + y +
                    ", z=" + z +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Vector vector = (Vector) o;

            if (x != vector.x) return false;
            if (y != vector.y) return false;
            return z == vector.z;

        }

        @Override
        public int hashCode() {
            int result = (int) (x ^ (x >>> 32));
            result = 31 * result + (int) (y ^ (y >>> 32));
            result = 31 * result + (int) (z ^ (z >>> 32));
            return result;
        }
    }
}
