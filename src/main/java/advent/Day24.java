package advent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.Pack200;
import java.util.stream.Collectors;

/**
 * Created by joe on 02.12.17.
 */
public class Day24 {

    public static int currentMaxStrength;
    public static int longestBridgeSize = 0;

    public static void main(String[] args) {
        String inputFilename = "Day24example.txt";
        doDay24(inputFilename);
        doDay24("Day24.txt");
    }

    private static void doDay24(String inputFilename) {
        Dataset dataset = new Dataset(inputFilename);
        List<String> lines = dataset.getLines();
        List<Component> componentList = new ArrayList<>();
        List<Component> startingComponents = new ArrayList<>();
        Map<Integer, List<Component>> portToComponentMap = new HashMap<>();

        for(String line : lines){
            String[] split = line.split("/");
            Component component = new Component(split[0], split[1]);
            componentList.add(component);

            //If it is possible to start with this one, make a note of that.
            if(component.sideA == 0 || component.sideB == 0){
                startingComponents.add(component);
            }
        }

        Set<Component> collect = componentList.stream().collect(Collectors.toSet());
//        System.out.println("Set size: " + collect.size());


        buildBridge(componentList, new LinkedHashSet<>(), 0, 0);
        System.out.println("MaxStrength: " + currentMaxStrength);
    }

    //Assumes that one of them is already in use, and returns the size of the other one.
    private static int getAvailablePortSize(Component c, int portSizeInUse) {
        return c.sideA == portSizeInUse ? c.sideB : c.sideA;
    }


    public static int buildBridge(List<Component> allComponents, Set<Component> usedComponents, int requiredPortSize, int bridgeStrength){

        for (int i = 0; i < allComponents.size(); i++) {
            Component currentComponent = allComponents.get(i);
            if (usedComponents.contains(currentComponent) || !currentComponent.hasPortOfSize(requiredPortSize)) {
                continue;
            }

            usedComponents.add(currentComponent);
            int bridgeLength = usedComponents.size();
            if(bridgeLength > longestBridgeSize) {
                longestBridgeSize = bridgeLength;
                currentMaxStrength = usedComponents.stream().map(Component::getStrength).mapToInt(Integer::intValue).sum();
            } else if (bridgeLength == longestBridgeSize){
                bridgeStrength = usedComponents.stream().map(Component::getStrength).mapToInt(Integer::intValue).sum();
                if(bridgeStrength > currentMaxStrength){
//                System.out.println(usedComponents + "  " + currentComponent);
                    currentMaxStrength = bridgeStrength;
                }
            }

            buildBridge(allComponents, usedComponents, getAvailablePortSize(currentComponent, requiredPortSize), bridgeStrength);
            usedComponents.remove(currentComponent);
        }

        return 0;
    }


    private static class Component {

        private final int sideA;
        private final int sideB;

        public Component(String sideA, String sideB) {
            this.sideA = Integer.parseInt(sideA);
            this.sideB = Integer.parseInt(sideB);
        }

        public int getStrength(){
            int strength = sideA + sideB;
//            System.out.println(String.format("Strength of %s + %s = %s", sideA, sideB, strength));
            return strength;
        }

        public boolean hasPortOfSize(int requiredSize){
            return sideA == requiredSize || sideB == requiredSize;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Component component = (Component) o;

            if (sideA != component.sideA) return false;
            return sideB == component.sideB;

        }

        @Override
        public int hashCode() {
            int result = sideA;
            result = 31 * result + sideB;
            return result;
        }

        @Override
        public String toString() {
            return "{" +
                    "" + sideA +
                    "/" + sideB +
                    '}';
        }
    }
}
