package advent;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URL;
import java.util.List;

/**
 * Created by joe on 27.09.17.
 */
public class Dataset {


    private final String name;

    public Dataset(String name) {
        this.name = name;
    }

    public String asString(){
        URL systemResourceAsStream = ClassLoader.getSystemResource(name);
        try {
            return FileUtils.readFileToString(new File(systemResourceAsStream.toURI())).trim();
        } catch (Exception e) {
            throw new RuntimeException("Couldn't load dataset [" + name + "]. You must have done something wrong...");
        }
    }

    public String asSingleLineString(){
        return asString().replaceAll(System.lineSeparator(), "");
    }

    public List<String> getLines(){
        URL systemResourceAsStream = ClassLoader.getSystemResource(name);
        try {
            return FileUtils.readLines(new File(systemResourceAsStream.toURI()));
        } catch (Exception e) {
            throw new RuntimeException("Couldn't load dataset [" + name + "]. You must have done something wrong...");
        }
    }

    public int[][] as2dIntArray(){

        List<String> lines = getLines();
        int size = lines.size();
        int [][] a = new int [size][];
        for(int i = 0; i < size; i++){
            String line = lines.get(i);
            String[] split = line.split("\\s+");
            for(int j = 0; j < split.length; j++){
                int intVal = Integer.parseInt(split[j].trim());
                a[i][j] = intVal;
            }
        }
        return a;
    }

    public int[] asIntArray(){
        List<String> lines = getLines();
        int [] a = new int[lines.size()];
        for(int i = 0; i < lines.size(); i ++){
            a[i] = Integer.parseInt(lines.get(i).trim());
        }
        return a;
    }

    public byte[] getBytes() {
        return asString().getBytes();
    }
}
