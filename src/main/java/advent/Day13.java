package advent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * Created by joe on 02.12.17.
 */
public class Day13 {


    public static void main(String[] args) {

        List<String> lines = new Dataset("Day13.txt").getLines();
        System.out.println("Part1: " + part1(lines));
        System.out.println("Part2: " + part2(lines));
    }


    public static int part1(List<String> lines){
        Map<Integer, Integer> layers = getLayerMap(lines);
        return computeSeverity(layers, 0, false);
    }

    public static int part2(List<String> lines){
        Map<Integer, Integer> layers = getLayerMap(lines);
        return IntStream.rangeClosed(0, Integer.MAX_VALUE)
                .filter(delay -> computeSeverity(layers, delay, true) == 0)
                .findFirst()
                .getAsInt();
    }

    private static Map<Integer, Integer> getLayerMap(List<String> lines) {
        Map<Integer, Integer> layers = new HashMap<>();
        for (int i = 0; i < lines.size(); i++) {
            String firewallDesc = lines.get(i);
            String[] firewallParts = firewallDesc.split(": ");
            int depth = Integer.parseInt(firewallParts[0]);
            int range = Integer.parseInt(firewallParts[1]);
            layers.put(depth, range);
        }
        return layers;
    }

    private static int computeSeverity(Map<Integer, Integer> layers, int delay, boolean exitIfDetected) {
        int lastLayer = layers.keySet().stream().mapToInt(Integer::intValue).max().getAsInt();
        int severity = 0;

        for(int i = 0; i <= lastLayer; i++){

            Integer range = layers.getOrDefault(i, 0);

            if(range == 0){
                continue;
            }
            int currentIndex;
            int positionInCycle = (i + delay) % ((2*range) - 2);
            if(positionInCycle < range){
                currentIndex = positionInCycle;
            } else {
                currentIndex = range - 2 + (range - positionInCycle);
            }

            if(currentIndex == 0){
                if(exitIfDetected) return 1; // Small optimization
                severity += (i + delay) * range;
            }


        }
        return severity;
    }
}
