package advent;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by joe on 02.12.17.
 */
public class Day18 {


    private final Long pid;
    private final LinkedList<Long> sendQueue;
    private final LinkedList<Long> receiveQueue;
    private int sendCount;

    public Day18(Long pid,
                 LinkedList<Long> queueB,
                 LinkedList<Long> queueA) {

        this.sendQueue = queueB;
        this.receiveQueue = queueA;
        this.pid = pid;
    }

    public static void main(String[] args) {

        List<String> instructions = new Dataset("Day18.txt").getLines();

        LinkedList<Long> queueA = new LinkedList<>();
        LinkedList<Long> queueB = new LinkedList<>();

        Day18 day180 = new Day18(0L, queueA, queueB);
        Day18 day181 = new Day18(1L, queueB, queueA);

        Arrays.asList(day180, day181)
                .parallelStream()
                .forEach(day18 -> {
                    try {
                        day18.process(instructions);
                    } catch (InterruptedException e) {
                        throw new RuntimeException("Crap.");
                    }
                });



        System.out.println(day181.sendCount);
    }


    private int process(List<String> lines) throws InterruptedException {

        Map<String, Long> registers = new HashMap<>();
        registers.put("p", pid);
        Long lastPlayed = 0L;

        for(int i = 0; i < lines.size(); i++){

            String line = lines.get(i);
            System.out.println();
            System.out.println(i + " : "  + line);
            String command = line.substring(0, 3);
            String input = line.substring(3, line.length()).trim();

            switch(command){
                case "snd":

                    Long value = getValue(registers, input);
                    sendQueue.addLast(value);
                    sendCount++;
                    System.out.println("SendCount for pid[" + pid + "] " + sendCount);
                    lastPlayed = value;
                    break;

                case "set":
                    BiOperation setOperands = new BiOperation(input);
                    registers.put(setOperands.registerName, getValue(registers, setOperands.valueOrRegisterName));
                    break;

                case "add":
                    BiOperation addOperands = new BiOperation(input);
                    registers.put(addOperands.registerName, getValue(registers, addOperands.registerName) + getValue(registers, addOperands.valueOrRegisterName));
                    break;
                case "mul":
                    BiOperation mulOperands = new BiOperation(input);
                    registers.put(mulOperands.registerName,
                                  getValue(registers, mulOperands.registerName) * getValue(registers, mulOperands.valueOrRegisterName));
                    break;
                case "mod":
                    BiOperation modOperands = new BiOperation(input);
                    System.out.println(modOperands.registerName + " " + modOperands.valueOrRegisterName);
                    registers.put(modOperands.registerName,
                                  getValue(registers, modOperands.registerName) % getValue(registers, modOperands.valueOrRegisterName));
                    break;
                case "rcv":
                    Long rcvRegisterVal;

                    do {
                        Thread.yield();
                        rcvRegisterVal = receiveQueue.poll();
                    } while (rcvRegisterVal == null);

                    registers.put(input, rcvRegisterVal);

//                    if(rcvRegisterVal == 0){
//                        System.out.println(String.format("Skipping rcv because val of %s is = %s", input, rcvRegisterVal));
//                    } else {
//                        System.out.println("Recovered last played sound because value of " + input + " was " + rcvRegisterVal + ". lastPlayed: " + lastPlayed);
////                        if(lastPlayed == 0){
////                            System.out.println("LastPlayed noise here was 0...");
////                        } else {
////                            throw new RuntimeException("Recovered " + lastPlayed);
////                        }
//                    }

                    break;
                case "jgz":
                    BiOperation jumpOperands = new BiOperation(input);
                    Long jgzRegisterVal = getValue(registers, jumpOperands.registerName);
                    if(jgzRegisterVal > 0){
                        Long offset = getValue(registers, jumpOperands.valueOrRegisterName);
                        i += (offset - 1);
                        System.out.println("Jumping " + offset);
                    } else {
                        System.out.println(String.format("Skipping jgz because val of %s is %s <= 0", jumpOperands.registerName, jgzRegisterVal));
                    }
                    break;
            }

            System.out.println(registers);
        }

        return 0;
    }

    private static class BiOperation {

        private String registerName;
        private String valueOrRegisterName;

        public BiOperation(String input) {
            System.out.println(String.format("Creating BiOp from %s", input));
            String[] split = input.trim().split(" ");
            this.registerName = split[0].trim();
            this.valueOrRegisterName = split[1].trim();
        }
    }


    private static Long getValue(Map<String, Long> registers,
                                String input) {
        String trimmedInput = input.trim();
        if (trimmedInput.matches("(-)?\\d+")){
            System.out.println("Treating " + input + " as a number");
            return Long.parseLong(trimmedInput);
        }
        System.out.println("Treating " + input + " as a register");
        return registers.getOrDefault(trimmedInput, 0L);
    }
}
