package advent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by joe on 02.12.17.
 */
public class Day16 {


    public static void main(String[] args) {

        String moves = new Dataset("Day16.txt").asSingleLineString();
        char [] chars = new char[16];
        for (char i = 0; i < chars.length; i++) {
            chars[i] = (char)('a' + i);
        }
        char[] result = run(chars, moves);
        System.out.println(Arrays.toString(result));
        for(Character c : result){
            System.out.print(c);
        }
    }

    private static char[] run(char [] input, String moves) {
        String[] moveList = moves.split(",");
        List<Move> moveFunctions = new ArrayList<>();
        for(String move : moveList){
            if(move.startsWith("s")){
                int elementsToMove = Integer.parseInt(move.substring(1));
                moveFunctions.add(new Move.Spin(elementsToMove));
            } else if(move.startsWith("x")){
                String[] split = move.substring(1).split("/");
                int posA = Integer.parseInt(split[0]);
                int posB = Integer.parseInt(split[1]);
                moveFunctions.add(new Move.PositionSwap(posA, posB));
            } else if(move.startsWith("p")){
                String[] split = move.substring(1).split("/");
                char charA = split[0].charAt(0);
                char charB = split[1].charAt(0);
                moveFunctions.add(new Move.ValueSwap(charA, charB));
            } else {
                throw new RuntimeException("Suprise move! " + move);
            }

        }

        char [] currentState = input;
        int iterationDenominator = -1;
        for (int i = 1; i <= 1_000_000_000; i++) {
            for(int functionIndex = 0; functionIndex < moveFunctions.size(); functionIndex++){
                Move moveFunction = moveFunctions.get(functionIndex);
                //System.out.println("Applying " + moveFunction);
                currentState = moveFunction.apply(currentState);
            }

            if(Arrays.equals(currentState, input)){
                iterationDenominator = i;
                System.out.println(String.format("Seen it before after applying all functions %s times", i));
                int fullIterations = 1_000_000_000 / iterationDenominator;
                i = (fullIterations * iterationDenominator);
                System.out.println("Jumping to " + i);
            }
        }

        System.out.println(iterationDenominator);

//////        for(int functionIndex = 0; functionIndex < theLastFewMoves; functionIndex++){
//////            Move moveFunction = moveFunctions.get(functionIndex);
//////            System.out.println(Arrays.toString(currentState));
//////            System.out.println("Applying " + moveFunction);
//////            currentState = moveFunction.apply(currentState);
////        }
//        System.out.println(String.format("FullCycles[%s] LastMoves[%s] Total[%s]", fullIterations,theLastFewMoves, (fullIterations+theLastFewMoves)));

        return currentState;
    }


}
