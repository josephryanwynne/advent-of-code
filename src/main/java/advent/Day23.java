package advent;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by joe on 02.12.17.
 */
public class Day23 {




    private final Long pid;
    private final LinkedList<Long> sendQueue;
    private final LinkedList<Long> receiveQueue;
    private int sendCount;
    private int multiplications = 0;

    public Day23(Long pid,
                 LinkedList<Long> queueB,
                 LinkedList<Long> queueA) {

        this.sendQueue = queueB;
        this.receiveQueue = queueA;
        this.pid = pid;
    }

    public static void main(String[] args) {

        /**
         *  For part 2 see Day23Java.java!
         */

        List<String> instructions = new Dataset("Day23.txt").getLines();

        LinkedList<Long> queueA = new LinkedList<>();
        LinkedList<Long> queueB = new LinkedList<>();

        Day23 day180 = new Day23(0L, queueA, queueB);
        Day23 day181 = new Day23(1L, queueB, queueA);

        Arrays.asList(day180)
                .parallelStream()
                .forEach(day18 -> {
                    try {
                        day18.process(instructions);
                    } catch (InterruptedException e) {
                        throw new RuntimeException("Crap.");
                    }
                });


        //System.out.println(String.format("Multiplications: %s", day180.multiplications));
        //System.out.println(day181.sendCount);
    }


    private int process(List<String> lines) throws InterruptedException {

        Map<String, Long> registers = new HashMap<>();
        registers.put("p", pid);
        registers.put("a", 1L);
        Long lastPlayed = 0L;


        int [] commandExecutions = new int[32];

        for(int i = 0; i < lines.size(); i++){
            commandNumber++;
            printRegisters(registers);

            commandExecutions[i]++;

            System.out.println(String.format("Executions: " + Arrays.toString(commandExecutions)));

            String line = lines.get(i);
            //System.out.println();
            //System.out.println(i + " : "  + line);
            String command = line.substring(0, 3);
            String commandArgs = line.substring(3, line.length()).trim();

            switch(command){

//                set X Y sets register X to the value of Y.
//                sub X Y decreases register X by the value of Y.
//                mul X Y sets register X to the result of multiplying the value contained in register X by the value of Y.
//                jnz X Y jumps with an offset of the value of Y, but only if the value of X is not zero. (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)

                case "snd":

                    Long value = getValue(registers, commandArgs);
                    sendQueue.addLast(value);
                    sendCount++;
                    //System.out.println("SendCount for pid[" + pid + "] " + sendCount);
                    lastPlayed = value;
                    break;

                case "set":
                    BiOperation setOperands = new BiOperation(commandArgs);
                    registers.put(setOperands.registerName, getValue(registers, setOperands.valueOrRegisterName));
                    break;
                case "sub":
                    BiOperation subOperands = new BiOperation(commandArgs);
                    registers.put(subOperands.registerName, getValue(registers, subOperands.registerName) - getValue(registers, subOperands.valueOrRegisterName));
                    break;
                case "add":
                    BiOperation addOperands = new BiOperation(commandArgs);
                    registers.put(addOperands.registerName, getValue(registers, addOperands.registerName) + getValue(registers, addOperands.valueOrRegisterName));
                    break;
                case "mul":
                    BiOperation mulOperands = new BiOperation(commandArgs);
                    registers.put(mulOperands.registerName,
                            getValue(registers, mulOperands.registerName) * getValue(registers, mulOperands.valueOrRegisterName));
                    multiplications ++;
                    break;
                case "mod":
                    BiOperation modOperands = new BiOperation(commandArgs);
                    //System.out.println(modOperands.registerName + " " + modOperands.valueOrRegisterName);
                    registers.put(modOperands.registerName,
                            getValue(registers, modOperands.registerName) % getValue(registers, modOperands.valueOrRegisterName));
                    break;
                case "rcv":
                    Long rcvRegisterVal;

                    do {
                        Thread.yield();
                        rcvRegisterVal = receiveQueue.poll();
                    } while (rcvRegisterVal == null);

                    registers.put(commandArgs, rcvRegisterVal);

//                    if(rcvRegisterVal == 0){
//                        //System.out.println(String.format("Skipping rcv because val of %s is = %s", input, rcvRegisterVal));
//                    } else {
//                        //System.out.println("Recovered last played sound because value of " + input + " was " + rcvRegisterVal + ". lastPlayed: " + lastPlayed);
////                        if(lastPlayed == 0){
////                            //System.out.println("LastPlayed noise here was 0...");
////                        } else {
////                            throw new RuntimeException("Recovered " + lastPlayed);
////                        }
//                    }

                    break;
                case "jgz":
                    BiOperation jumpOperands = new BiOperation(commandArgs);
                    Long jgzRegisterVal = getValue(registers, jumpOperands.registerName);
                    if(jgzRegisterVal > 0){
                        Long offset = getValue(registers, jumpOperands.valueOrRegisterName);
                        i += (offset - 1);
                        //System.out.println("Jumping " + offset);
                    } else {
                        //System.out.println(String.format("Skipping jgz because val of %s is %s <= 0", jumpOperands.registerName, jgzRegisterVal));
                    }
                    break;
                case "jnz":
                    BiOperation jnOperands = new BiOperation(commandArgs);
                    Long jnRegisterVal = getValue(registers, jnOperands.registerName);
                    if(jnRegisterVal != 0){
                        Long offset = getValue(registers, jnOperands.valueOrRegisterName);
                        i += (offset - 1);
                        //System.out.println("Jumping " + offset);
                    } else {
                        //System.out.println(String.format("Skipping jnz because val of %s is %s == 0", jnOperands.registerName, jnRegisterVal));
                    }
                    break;
            }

            //System.out.println(registers);
        }

        return 0;
    }

    long commandNumber = 0;

    private void printRegisters(Map<String, Long> registers) {

        String [] registerValues = new String[8];
        String[] strings = {"a", "b", "c", "d", "e", "f", "g", "h"};
        for (int i = 0; i < strings.length; i++) {
            registerValues[i] = "" + registers.getOrDefault(strings[i], 0L);
        }
//        System.out.println(commandNumber + "," + String.join(",", registerValues));
    }

    private static class BiOperation {

        private String registerName;
        private String valueOrRegisterName;

        public BiOperation(String input) {
            //System.out.println(String.format("Creating BiOp from %s", input));
            String[] split = input.trim().split(" ");
            this.registerName = split[0].trim();
            this.valueOrRegisterName = split[1].trim();
        }
    }


    private static Long getValue(Map<String, Long> registers,
                                 String input) {
        String trimmedInput = input.trim();
        if (trimmedInput.matches("(-)?\\d+")){
            //System.out.println("Treating " + input + " as a number");
            return Long.parseLong(trimmedInput);
        }
        //System.out.println("Treating " + input + " as a register");
        return registers.getOrDefault(trimmedInput, 0L);
    }
}
