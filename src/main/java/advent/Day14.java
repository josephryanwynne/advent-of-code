package advent;

import java.util.Arrays;

/**
 * Created by joe on 02.12.17.
 */
public class Day14 {


    public static void main(String[] args) {

        String inputKey = "jzgqcdpd";

        boolean[][] grid = getGrid(inputKey);

        for (int i = 0; i < grid.length; i++) {
            System.out.println(Arrays.toString(grid[i]));
        }
        System.out.println("Part1: " + part1(grid));
        System.out.println("Part2: " + part2(grid));
    }

    public static int part1(boolean[][] grid) {
        int total = 0;
        for(int i = 0; i < grid.length; i++){
            for (int j = 0; j < grid[i].length; j++) {
                total += grid[i][j] ? 1 : 0;
            }
        }
        return total;
    }

    public static boolean[][] getGrid(String inputKey) {
        boolean[][] grid = new boolean[128][128];

        for(int i = 0; i < 128; i++){

            String hexHash = Day10.generateDay10Hash(inputKey + "-" + i);

            for (int hashPosition = 0; hashPosition < hexHash.length(); hashPosition += 4) {
                String hexPart = hexHash.substring(hashPosition, hashPosition + 4);

                for (int hexPartPosition = 0; hexPartPosition < 4; hexPartPosition++) {
                    int valueOfPart = Integer.parseInt("" + hexPart.charAt(hexPartPosition), 16);

                    int rowPosition = hashPosition * 4 + hexPartPosition * 4;

                    grid[i][rowPosition + 0] = ((valueOfPart & 0b1000) > 0);
                    grid[i][rowPosition + 1] = ((valueOfPart & 0b0100) > 0);
                    grid[i][rowPosition + 2] = ((valueOfPart & 0b0010) > 0);
                    grid[i][rowPosition + 3] = ((valueOfPart & 0b0001) > 0);
                }
            }
        }

        return grid;
    }

    public static int part2(boolean[][] grid) {

        int[][] regionMemberships = new int[grid.length][grid[0].length];

        int groupsSoFar = 0;

        for (int i = 0; i < grid.length; i++) {

            for (int j = 0; j < grid[i].length; j++) {
                //if cell is true and not already in a group
                if(grid[i][j] && regionMemberships[i][j] == 0){
                    //create a new groupId
                    groupsSoFar++;
                    //updateGroupsRecursively until we find only false or until we hit edges
                    addToGroup(grid, regionMemberships, i, j, groupsSoFar);
                }

            }
        }

        return groupsSoFar;
    }

    private static void addToGroup(boolean[][] grid, int[][] regionMemberships, int i, int j, int currentGroupId) {

        if(i < 0 || j < 0 || i == grid.length || j == grid[i].length //Boundaries
                || !grid[i][j] // Is the value set to 1?
                || regionMemberships[i][j] != 0){ // Have we already added this to the group?
            return;
        }
        regionMemberships[i][j] = currentGroupId;
        addToGroup(grid, regionMemberships, i-1, j, currentGroupId);
        addToGroup(grid, regionMemberships, i+1, j, currentGroupId);
        addToGroup(grid, regionMemberships, i, j-1, currentGroupId);
        addToGroup(grid, regionMemberships, i, j+1, currentGroupId);
    }

}
