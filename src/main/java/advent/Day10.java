package advent;

import java.util.stream.IntStream;

/**
 * Created by joe on 02.12.17.
 */
public class Day10 {

    public static void main(String[] args) {
        String input = new Dataset("Day10.txt").asString();
        System.out.println(generateDay10Hash(input));
    }

    public static String generateDay10Hash(String input) {
        int[] sparseHash = getSparseHash(IntStream.rangeClosed(0, 255).toArray(), getBytes(input));
        int[] denseHash = getDenseHash(sparseHash);
        String hashAsHex = intArrayToHexString(denseHash);
        return hashAsHex.toLowerCase();
    }

    public static String intArrayToHexString(int[] denseHash) {
        StringBuilder hashBuilder = new StringBuilder();
        for(int i : denseHash){
            hashBuilder.append(StringUtils.formatAsHex(i, 2));
        }
        return hashBuilder.toString();
    }

    public static int[] getDenseHash(int[] sparseHash) {
        int[] denseHash = new int[sparseHash.length / 16];
        for (int i = 0; i < sparseHash.length; i+=16) {
            int denseHashValue = ArrayUtils.xorSubArray(sparseHash, i, i + 16);
            denseHash[i/16] = denseHashValue;
        }
        return denseHash;
    }

    public static byte[] getBytes(String input) {
        byte[] chars = input.getBytes();
        byte[] salt = new byte[]{17, 31, 73, 47, 23};
        byte[] bytes = new byte[chars.length+salt.length];
        System.arraycopy(chars, 0, bytes, 0, chars.length);
        System.arraycopy(salt, 0, bytes, chars.length, salt.length);
        return bytes;
    }

    public static int[] getSparseHash(int[] hashArray, byte[] inputLengths) {

        int currentPos = 0, skipSize = 0;

        for(int round = 0; round < 64; round++) {
            for (int i = 0; i < inputLengths.length; i++) {
                int currentLength = inputLengths[i];
                for (int j = 0; j < currentLength / 2; j++) {
                    int swapPosA = (currentPos + j) % hashArray.length;
                    int swapPosB = (currentPos + currentLength - j - 1) % hashArray.length;
                    ArrayUtils.swap(hashArray, swapPosA, swapPosB);
                }

                currentPos = (currentPos + currentLength + skipSize++) % hashArray.length;
            }
        }
        return hashArray;
    }

}
