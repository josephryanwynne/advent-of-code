package advent;

/**
 * Created by joe on 09.12.17.
 */
public class Day9 {

    public static void main(String[] args) {
        run("Day9.txt");
    }

    private static void run(String filename){
        String input = new Dataset(filename).asString();

        System.out.println(getScore(input));

    }

    public static int getScore(String input) {

        int depth = 1;
        int score = 0;
        int garbages = 0;
        boolean garbage = false;
        for(int i = 0; i < input.length(); i++){

            String substring = input.substring(i);

            if(substring.startsWith("!")){
                i++;
            } else if(!garbage && substring.startsWith("<")) {
                garbage = true;
            } else if(substring.startsWith(">")) {
                garbage = false;
            } else if(!garbage && substring.startsWith("{")) {
                score += depth++;
            } else if(!garbage && substring.startsWith("}")) {
                depth--;
            } else if(garbage){
                garbages++;
            }

        }

        return garbages;
    }


}
