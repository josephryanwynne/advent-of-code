package advent;

import com.google.common.collect.ImmutableMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joe on 02.12.17.
 */
public class Day11 {

    //Using 6 element enum might make this actually possible / readable ?
    static int NORTH = 0;
    static int NORTH_EAST = 1;
    static int SOUTH_EAST = 2;
    static int SOUTH = 3;
    static int SOUTH_WEST = 4;
    static int NORTH_WEST = 5;
    static final Map<String, Integer> directionIndexes = new HashMap<>();


    static {
        //Need a better map init util
        directionIndexes.put("n", NORTH);
        directionIndexes.put("ne", NORTH_EAST);
        directionIndexes.put("se", SOUTH_EAST);
        directionIndexes.put("s", SOUTH);
        directionIndexes.put("sw", SOUTH_WEST);
        directionIndexes.put("nw", NORTH_WEST);
    }

    public static void main(String[] args) {

        String input = new Dataset("Day11.txt").asString();
        System.out.println(getSteps(input));
    }

    private static int getSteps(String input) {
        int times = 0;
        String[] moves = input.split(",");

        int[] hexMex = new int[6];
        int maxDistance = 0;
        for(String move : moves) {

            // Put all the moves into a hexy thing
            hexMex[directionIndexes.get(move)]++;

            boolean stillNeedsReducing;

            do {
                int[] stateBeforeReduction = new int[6];
                System.arraycopy(hexMex, 0, stateBeforeReduction, 0, 6);
                for (int currentDirectionIndex = 0; currentDirectionIndex < 6; currentDirectionIndex++) {
                    System.out.println("Starting iteration " + currentDirectionIndex + " " + Arrays.toString(hexMex));
                    int oppositeDirectionIndex = (currentDirectionIndex + 3) % 6;
                    int thisDirectionVal = hexMex[currentDirectionIndex];
                    int oppositeDirectionVal = hexMex[oppositeDirectionIndex];


                    System.out.println(String.format("Current[%s] Opposite[%s]", thisDirectionVal, oppositeDirectionVal));
                    if (thisDirectionVal >= oppositeDirectionVal) {
                        hexMex[currentDirectionIndex] -= oppositeDirectionVal;
                        hexMex[oppositeDirectionIndex] = 0;
                    } else {
                        hexMex[oppositeDirectionIndex] -= thisDirectionVal;
                        hexMex[currentDirectionIndex] = 0;
                    }
                    System.out.println("After opposite reduction" + Arrays.toString(hexMex));

                    int previousDirectionIndex = (6 + currentDirectionIndex - 1) % 6;
                    int nextDirectionIndex = (currentDirectionIndex + 1) % 6;
                    int nextDirectionVal = hexMex[nextDirectionIndex];
                    int previousDirectionVal = hexMex[previousDirectionIndex];
                    if (previousDirectionVal > 0 && nextDirectionVal > 0) {
                        if (previousDirectionVal >= nextDirectionVal) {
                            hexMex[currentDirectionIndex] += nextDirectionVal;
                            hexMex[previousDirectionIndex] -= nextDirectionVal;
                            hexMex[nextDirectionIndex] = 0;
                        } else {
                            hexMex[currentDirectionIndex] += previousDirectionVal;
                            hexMex[nextDirectionIndex] -= previousDirectionVal;
                            hexMex[previousDirectionIndex] = 0;
                        }
                    }
                    System.out.println("After all reduction" + Arrays.toString(hexMex));
                    System.out.println();
                }

                System.out.println("Before" + Arrays.toString(stateBeforeReduction) + " After" + Arrays.toString(hexMex) + "");
                System.out.println("Next one...");
                stillNeedsReducing = !Arrays.equals(stateBeforeReduction, hexMex);
                times++;
            } while (stillNeedsReducing);

            System.out.println("Times: " + times);
            int currentDistance = Arrays.stream(hexMex).sum();
            System.out.println("Sum of all remaining directions: " + currentDistance);
            if(currentDistance > maxDistance){
                maxDistance = currentDistance;
            }
        }

        return maxDistance;
    }

}
