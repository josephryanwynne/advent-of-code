package advent;

import java.util.Arrays;

/**
 * Created by joe on 02.12.17.
 */
public class Day3 {

    public static void main(String[] args) {
        System.out.println(guess(265149));
    }

    public static int guess(int max){

        //Not sure why I ended up with this mess at the start.
        Double sqrtDbl = Math.ceil(Math.sqrt(max));
        int sqrt = sqrtDbl.intValue();
        int [][] grid = new int [sqrt+1][sqrt+1];
        int startingPoint = (sqrt % 2 == 0 ? sqrt / 2 : (sqrt - 1) / 2);


        int xPosition = startingPoint;
        int yPosition = startingPoint;
        grid[xPosition][yPosition] = 1;
        Move nextMove = Move.RIGHT;

        for (int i = 2; i <= max; i ++){

            xPosition += nextMove.xShift;
            yPosition += nextMove.yShift;

            int cellValue = getSumOfSurroundingCells(grid, xPosition, yPosition);

            if(cellValue > max){
                System.out.println("Found it! " + cellValue);
                break;
            } else {
                grid[xPosition][yPosition] = cellValue; //updated for part 2
            }

            //Check if we can turn in yet?
            Move nextSpiralMove = nextMove.getSpiralDirection();
            if(grid[xPosition+nextSpiralMove.xShift][yPosition+nextSpiralMove.yShift] == 0){
                nextMove = nextSpiralMove;
            }

        }

        int x = Math.abs(xPosition - startingPoint);
        int y = Math.abs(yPosition - startingPoint);

        for(int [] array : grid){
            System.out.println(Arrays.toString(array));
        }

        return x + y;

    }

    private static int getSumOfSurroundingCells(int[][] grid, int xPosition, int yPosition) {
        return    grid[xPosition][yPosition+1]
                + grid[xPosition][yPosition-1]
                + grid[xPosition+1][yPosition+1]
                + grid[xPosition+1][yPosition-1]
                + grid[xPosition-1][yPosition+1]
                + grid[xPosition-1][yPosition-1]
                + grid[xPosition-1][yPosition]
                + grid[xPosition+1][yPosition];
    }

    private enum Move {

        RIGHT(1, 0), UP(0, 1), LEFT(-1, 0), DOWN(0, -1);

        private int xShift;
        private int yShift;

        Move(int xShift, int yShift) {
            this.xShift = xShift;
            this.yShift = yShift;
        }

        Move getSpiralDirection(){
            return Move.values()[(this.ordinal() + 1) % Move.values().length];
        }
    }

}
