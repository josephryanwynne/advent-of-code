package advent;

import java.util.Arrays;

/**
 * Created by joe on 02.12.17.
 */
public class Day5 {

    public static void main(String[] args) {

        int[] input = new Dataset("Day5.txt").asIntArray();
        int index = 0, steps = 0;

        while (index >= 0 && index < input.length) {
            if (input[index] >= 3) {
                index += input[index]--;
            } else {
                index += input[index]++;
            }
            steps++;
        }
        System.out.println(Arrays.toString(input));
        System.out.println(steps);
    }

    static final int[] test = new int[]{0,
            3,
            0,
            1,
            -3};
}