package advent;

import java.math.BigInteger;

/**
 * Created by joe on 02.12.17.
 */
public class Day15 {


    public static void main(String[] args) {

        int startA = 883;
        int factorA = 16807;
        int startB = 879;
        int factorB = 48271;

        int result = run(startA, factorA, startB, factorB);
        System.out.println(result);
    }

    public static int run(int startA, int factorA, int startB, int factorB) {


        long currentA = startA;
        long currentB = startB;
        int matches = 0;

        boolean newA = false;
        boolean newB = false;

        int comparisons = 0;
        while(comparisons < 5_000_000) {

            while(!newA){
                currentA = generate(currentA, factorA);
                if(currentA % 4 == 0){
//                    System.out.println(currentA);
                    newA = true;
                }
            }

            while(!newB){
                currentB = generate(currentB, factorB);
                if(currentB % 8 == 0){
//                    System.out.println(currentB);
                    newB = true;
                }
            }


            comparisons++;
            boolean match = rightmost16BitsEqual(currentA, currentB);
            matches += match ? 1 : 0;
//            System.out.println(match);
            newA = false;
            newB = false;

        }

        return matches;
    }

    public static boolean rightmost16BitsEqual(long currentA, long currentB) {


        char currentA1 = (char) currentA;
        char currentB1 = (char) currentB;
//        System.out.println(String.format("A[%16s] B[%16s]",
//                toBinary(currentA1),
//                toBinary(currentB1)
//        ));
        return currentA1 == currentB1;
    }

    private static String toBinary(long currentA1) {
        return String.format("%32s", Integer.toBinaryString((int) currentA1)).replace(' ', '0');
    }

    public static long generate(long currentA, long factorA) {

        long result = currentA * factorA;

        if(1352636452 == currentA){
            throw new RuntimeException("Should have compared this.");
        }

        //Check for overflow if it stops working
        if(result < currentA){
            throw new RuntimeException("Overflowed AF");
        }
        long modded = result % Integer.MAX_VALUE;

        return modded;
    }
}
