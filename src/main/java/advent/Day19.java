package advent;

import java.util.Arrays;
import java.util.List;

/**
 * Created by joe on 02.12.17.
 */
public class Day19 {


    public static void main(String[] args) {

        List<String> lines = new Dataset("Day19.txt").getLines();
        System.out.println(run(lines));
    }


    public static String run(List<String> lines){

        int gridWidth = lines.get(0).length();
        int gridHeight = lines.size();

        char [][] grid = new char[gridHeight][gridWidth];

        for (int i = 0; i < lines.size(); i++) {
            char[] chars = lines.get(i).toCharArray();

            for (int j = 0; j < chars.length; j++) {
                grid[i][j] = chars[j];
            }
        }


        for (int i = 0; i < grid.length; i++) {
            System.out.println(Arrays.toString(grid[i]));
        }

        int startIndex = findStartIndex(grid);

        System.out.println("Starting from " + startIndex);

        StringBuilder sb = new StringBuilder();
        int x = 0, y = startIndex;
        Move moveDirection = Move.DOWN;

        Character currentChar;
        int steps = 0;
        do {
            steps ++;
            System.out.println(x + "," + y);


            currentChar = grid[x][y];

            if(currentChar >= 'A' && currentChar <= 'Z'){
                sb.append(currentChar);
            } else if (currentChar.equals('+')){

                System.out.println("Current char is a +");

                for(Move move : moveDirection.getSides()){

                    int lookAheadX = x + move.xShift;
                    int lookaheadY = y + move.yShift;

                    if(lookAheadX < 0
                            || lookAheadX >= gridHeight
                            || lookaheadY < 0
                            || lookaheadY >= gridWidth){
                        continue;
                    }
                    if(grid[lookAheadX][lookaheadY] == move.representation
                            || (grid[lookAheadX][lookaheadY] >= 'A' && grid[lookAheadX][lookaheadY] <= 'Z')){
                        moveDirection = move;
                    }

                }

            }
            int nextX = x + moveDirection.xShift;
            int nextY = y + moveDirection.yShift;
            x = nextX;
            y = nextY;
        } while(!currentChar.equals(' '));

        System.out.println(steps);
        return sb.toString();
    }

    private static int findStartIndex(char[][] grid) {
        return new String(grid[0]).indexOf("|");
    }

    private enum Move {

        RIGHT(0, 1, '-'), UP(-1, 0, '|'), LEFT(0, -1, '-'), DOWN(1, 0, '|');

        private final char representation;
        private int xShift;
        private int yShift;

        Move(int xShift, int yShift, char representation) {
            this.xShift = xShift;
            this.yShift = yShift;
            this.representation = representation;
        }

        List<Move> getSides(){
            return Arrays.asList(
                values()[(this.ordinal()+values().length-1) % values().length],
                values()[(this.ordinal()+1) % values().length]
            );
        }
    }
}
