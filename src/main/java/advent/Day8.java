package advent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joe on 02.12.17.
 */
public class Day8 {

    public static void main(String[] args) {
        run("Day8example.txt");
        run("Day8.txt");
    }

    private static void run(String inputFilename) {
        Dataset dataset = new Dataset(inputFilename);

        List<String> lines = dataset.getLines();

        int maxSeen = 0;
        Map<String, Integer> registers = new HashMap<>();
        for (String line : lines){
            String[] split = line.split(" ");
            if(split.length != 7 )throw new RuntimeException("Not all 7");

            Integer registerValue = registers.getOrDefault(split[0], 0);

            int valueChange = Integer.parseInt(split[2]);


            boolean execute = evaluate(registers, split[4], split[5], split[6]);

            if(execute) {
                int newValue;
                if (split[1].equals("inc")) {
                    newValue = registerValue + valueChange;
                } else {
                    newValue = registerValue - valueChange;
                }

                if(newValue > maxSeen){
                    maxSeen = newValue;
                }
                registers.put(split[0], newValue);
            }

        }

        int max = registers.values().stream().mapToInt(Integer::intValue).max().getAsInt();

        System.out.println("Max Register Value: " + max);
        System.out.println("Max Register Value During Execution: " + maxSeen);

    }

    private static boolean evaluate(Map<String, Integer> registers, String register, String op, String valString) {

        int registerValue = registers.getOrDefault(register, 0);
        int value = Integer.parseInt(valString);

        switch(op){
            case ">": return registerValue > value;
            case ">=": return registerValue >= value;
            case "<": return registerValue < value;
            case "<=": return registerValue <= value;
            case "==": return registerValue == value;
            case "!=": return registerValue != value;
            default: throw new RuntimeException("New Op: " + op);
        }
    }


}
