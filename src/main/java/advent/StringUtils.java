package advent;

/**
 * Created by joe on 10.12.17.
 */
public class StringUtils {

    public static String formatAsHex(int value, int hexStringWidth) {
        return String.format("%02X", value);
    }
}
