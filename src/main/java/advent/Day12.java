package advent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by joe on 02.12.17.
 */
public class Day12 {

    private static final String REGEX = "^([0-9]+) <-> (.*)$";


    public static void main(String[] args) {

        List<String> lines = new Dataset("Day12.txt").getLines();

        System.out.println("Zero Group Size: " + run(lines));

    }

    public static int run(List<String> lines) {

        HashMap<String, Node> nodesByName = new HashMap<>();
        for (String line : lines) {
            addNodeToTree(line, nodesByName);
        }

        Node zeroNode = null;
        for (Node nodeInMap : nodesByName.values()) {
            if (nodeInMap.name.equals("0")) {
                zeroNode = nodeInMap;
                break;
            }
        }
        System.out.println("" + zeroNode);

        Set<Node> seenInZeroGroup = new HashSet<>();
        addLinksByChildren(seenInZeroGroup, zeroNode);


        Set<Node> seen = new HashSet<>();
        int groups = 0;
        for(Node n : nodesByName.values()){
            if(!seen.contains(n)){
                groups ++;
                addLinksByChildren(seen, n);
            }
        }
        System.out.println("Total number of groups: " + groups);

        System.out.println("Size of zero group: " + seenInZeroGroup.size());

//        Node root = nodesByName.values().stream().filter(node -> node.parent == null).findAny().get();
//        System.out.println("Root : " + root.name);


        return seenInZeroGroup.size();
    }

    private static void addLinksByChildren(Set<Node> seen, Node currentNode) {

        if (!seen.contains(currentNode)) {
            seen.add(currentNode);
            List<Node> children = currentNode.children;
            List<Node> unseenChildren = getUnseenChildren(seen, children);
            for (Node unseenChild : unseenChildren) {
                addLinksByChildren(seen, unseenChild);
            }
        }
    }

    private static List<Node> getUnseenChildren(Set<Node> seen, List<Node> children) {

        List<Node> unseen = new ArrayList<>();
        for (Node child : children) {
            if (!seen.contains(child)) unseen.add(child);
        }
        return unseen;
    }

    private static void addNodeToTree(String line,
                                      Map<String, Node> nodes) {

//        bkdtinl (1167) -> ojsjuts, euoclfs, xbkeua, mykrcq, jjsvfy, aazxafl
        Pattern compile = Pattern.compile(REGEX);
        Matcher matcher = compile.matcher(line);
        if (matcher.matches()) {
            String name = matcher.group(1);
            String childIds = matcher.group(2);

            List<Node> children = new ArrayList<>();
            String[] split = childIds.split(",");
            for (int i = 0; i < split.length; i++) {
                String id = split[i].trim();


                if (id.isEmpty()) {
                    throw new RuntimeException("Wasn't expecting this!");
                }

                if (nodes.containsKey(id)) {
                    Node node = nodes.get(id);
                    children.add(node);
                } else {

                    Node child = new Node(id);
                    nodes.put(id, child);
                    children.add(child);
                }

            }
            Node thisNode;
            if (nodes.containsKey(name)) {
                Node node = nodes.get(name);
                node.setChildren(children);
                thisNode = node;
            } else {
                thisNode = new Node(null, name, children);
                nodes.put(thisNode.name, thisNode);
            }

            children.stream().forEach(child -> {
                if(!child.equals(thisNode)){
                    child.children.add(thisNode);
                }

            });

        } else {
            System.out.println("Can't parse: " + line);
            throw new RuntimeException("All lines should match");
        }
    }


    static class Node {

        Node parent;
        String name = "default";
        List<Node> children = new ArrayList<>();


        public Node(Node parent,
                    String name,
                    List<Node> children) {
            this.parent = parent;
            this.name = name;
            this.children = children;
        }

        public Node(String name) {
            this.name = name;
        }

        public void setParent(Node parent) {
            this.parent = parent;
        }

        public void setChildren(List<Node> children) {
            this.children = children;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "name='" + name + '\'' +
                    '}';
        }

        public boolean hasChildren() {

            return !children.isEmpty();
        }
    }
}
