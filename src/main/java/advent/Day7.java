package advent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by joe on 02.12.17.
 */
public class Day7 {


    private static final String REGEX = "^([a-z]+) \\(([0-9]+)\\)( -> )?(.*)$";

    public static void main(String[] args) {
        List<String> lines = new Dataset("Day7.txt").getLines();


        HashMap<String, Node> nodesByName = new HashMap<>();
        for (String line : lines) {
            addNodeToTree(line, nodesByName);
        }

        Node root = nodesByName.values().stream().filter(node -> node.parent == null).findAny().get();
        System.out.println("Root : " + root.name);
        getCombinedWeight(root);

    }

    private static int getCombinedWeight(Node currentNode) {

        if (currentNode.hasChildren()) {

            List<Node> children = currentNode.children;
            List<Integer> childWeights = children.stream().map(child -> getCombinedWeight(child)).collect(
                    Collectors.toList());


            Map<Integer, List<Integer>> childWeightMap = childWeights.stream().collect(
                    Collectors.groupingBy(Function.identity()));

            if (childWeightMap.size() > 1) {
                Collection<List<Integer>> values = childWeightMap.values();
                Iterator<List<Integer>> iterator = values.iterator();
                List<Integer> a = iterator.next();
                List<Integer> b = iterator.next();
                int badValue, goodValue;
                if (a.size() > 1) {
                    goodValue = a.get(0);
                    badValue = b.get(0);
                } else {
                    goodValue = b.get(0);
                    badValue = a.get(0);
                }

                int badGuyIndex = childWeights.indexOf(badValue);
                int badGuyWeight = children.get(badGuyIndex).weight;

                // Weight needs to be current weight + whatever the difference is.
                int difference = goodValue - badValue;
                int desiredWeight = badGuyWeight + difference;

                System.out.println("Difference: " + difference);
                System.out.println("GoodValue: " + goodValue);
                System.out.println("BadValue: " + badValue);
                System.out.println("DesiredWeight: " + desiredWeight);

            }

            int totalChildWeight = childWeights.stream().mapToInt(Integer::intValue).sum();
            return totalChildWeight + currentNode.weight;
        }
        return currentNode.weight;
    }

    private static void addNodeToTree(String line,
                                      Map<String, Node> nodes) {

//        bkdtinl (1167) -> ojsjuts, euoclfs, xbkeua, mykrcq, jjsvfy, aazxafl
        Pattern compile = Pattern.compile(REGEX);
        Matcher matcher = compile.matcher(line);
        if (matcher.matches()) {
            String name = matcher.group(1);
            String weightString = matcher.group(2);
            int weight = Integer.parseInt(weightString);
            String childIds = matcher.group(4);

            List<Node> children = new ArrayList<>();
            String[] split = childIds.split(",");
            for (int i = 0; i < split.length; i++) {
                String id = split[i].trim();

                if (id.isEmpty()) {
                    continue;
                }

                if (nodes.containsKey(id)) {
                    Node node = nodes.get(id);
                    children.add(node);
                } else {

                    Node child = new Node(id);
                    nodes.put(id, child);
                    children.add(child);
                }

            }
            Node thisNode;
            if (nodes.containsKey(name)) {
                Node node = nodes.get(name);
                node.setChildren(children);
                node.setWeight(weight);
                thisNode = node;
            } else {
                thisNode = new Node(null, name, weight, children);
                nodes.put(thisNode.name, thisNode);
            }
            for (Node child : children) {
                child.parent = thisNode;
            }
        } else {
            throw new RuntimeException("All lines should match");
        }
    }


    static class Node {

        Node parent;
        String name = "default";
        int weight;
        List<Node> children = new ArrayList<>();


        public Node(Node parent,
                    String name,
                    int weight,
                    List<Node> children) {
            this.parent = parent;
            this.name = name;
            this.weight = weight;
            this.children = children;
        }

        public Node(String name) {
            this.name = name;
        }

        public void setParent(Node parent) {
            this.parent = parent;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        public void setChildren(List<Node> children) {
            this.children = children;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "name='" + name + '\'' +
                    ", weight=" + weight +
                    '}';
        }

        public boolean hasChildren() {

            return !children.isEmpty();
        }
    }
}
